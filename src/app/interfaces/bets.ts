// tslint:disable:class-name
export interface BetTeamItem_Interface {
  name: string;
  win: number;
}

export interface BetTeam_interface {
  id: number;
  teams: BetTeamItem_Interface[];
  draw: number;
}

export interface ConnectorTeam_interface {
  [id: number]: number; // { [id]: index }
}
