import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {TeamsApiService} from '../../../services/teams-api.service';
import {BetTeam_interface, ConnectorTeam_interface} from '../../../interfaces/bets';
import {TeamSocketApiService} from '../../../services/team-socket-api.service';
import io from 'socket.io-client';
import {environment} from '../../../../environments/environment';


@Component({
  selector: 'app-teams-start',
  templateUrl: './teams-start.component.html',
  styleUrls: ['./teams-start.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,

})
export class TeamsStartComponent implements OnInit, OnDestroy {

  url = environment.apiUrl;
  socket = io(this.url);

  teams: BetTeam_interface[] = [];
  connectorTeamObject: ConnectorTeam_interface = {} ;

  constructor(
    private teamsApi: TeamsApiService,
    private teamSocketApi: TeamSocketApiService,
    private detect: ChangeDetectorRef
  ) { }


  ngOnInit(): void {
    this.getAllBets();

    this.socket.on('bet-updated', ( upTeams ) => {
      this.updateTeamsByConnectObject(upTeams);
      this.detect.detectChanges();
    });
  }


  ngOnDestroy(): void {
    this.socket.close();
  }


  getAllBets() {
    this.teamsApi.getAllBets().subscribe( el => {
      this.teams = el;
      this.createConnectingObject(this.teams);
      this.detect.detectChanges();
    });
  }


  trackFunction(index, item){
    if (!item) { return null; }
    return index;
  }


  updateTeamsByConnectObject(upTable: BetTeam_interface[]){
    upTable.forEach( el => {
      this.teams[this.connectorTeamObject[el.id]] = el;
    });
  }


  createConnectingObject(table: BetTeam_interface[]){
    this.connectorTeamObject = {};
    table.forEach( (item, index) => {
      this.connectorTeamObject = {...this.connectorTeamObject, [item.id]: index};
    });
  }


  generate(size= 100) {
    this.teamsApi.generateBets(size).subscribe( (el: any) => {
      console.log(el.ok ? `Generate team table size: ${size}` : null);
      this.getAllBets();
    });
  }


}
