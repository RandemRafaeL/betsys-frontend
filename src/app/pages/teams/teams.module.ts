import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsStartComponent } from './teams-start/teams-start.component';
import {RouterModule, Routes} from '@angular/router';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import {ControlPanelModule} from '../../components/control-panel/control-panel.module';

const routes: Routes = [
  {path: '' , component: TeamsStartComponent},
  {path: '**', redirectTo: '/teams', pathMatch: 'full'}
];

@NgModule({
  declarations: [TeamsStartComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FlexLayoutModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatToolbarModule,
    ControlPanelModule
  ]
})
export class TeamsModule { }
