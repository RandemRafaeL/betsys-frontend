import { TestBed } from '@angular/core/testing';

import { TeamSocketApiService } from './team-socket-api.service';

describe('TeamSocketApiService', () => {
  let service: TeamSocketApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TeamSocketApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
