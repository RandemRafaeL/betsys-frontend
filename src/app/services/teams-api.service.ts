import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {BetTeam_interface} from '../interfaces/bets';

@Injectable({
  providedIn: 'root'
})
export class TeamsApiService {

  url = environment.apiUrl;

  constructor( private http: HttpClient) { }

  getAllBets(): Observable<BetTeam_interface[]> {
    return this.http.get<BetTeam_interface[]>(`${this.url}/bets`);
  }

  getOneBet(id): Observable<BetTeam_interface> {
    return this.http.get<BetTeam_interface>(`${this.url}/bets/${id}` );
  }

  generateBets(size): Observable<BetTeam_interface> {
    return this.http.get<BetTeam_interface>(`${this.url}/bets-generate?size=${size}` );
  }

}
