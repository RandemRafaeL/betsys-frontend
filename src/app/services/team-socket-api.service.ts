import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeamSocketApiService {

  url = environment.apiUrl;

  constructor( private http: HttpClient) { }

  pullingStart(rate: number){
    return this.http.get(`${this.url}/pulling/start?rate=${rate}`);
  }

  pullingStop(){
    return this.http.get(`${this.url}/pulling/stop`);
  }

}
