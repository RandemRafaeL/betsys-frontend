import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: 'start', loadChildren: () => import('./pages/start/start.module').then( mod => mod.StartModule)},
  {path: 'teams', loadChildren: () => import('./pages/teams/teams.module').then( mod => mod.TeamsModule)},
  {path: '**', redirectTo: '/start', pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
