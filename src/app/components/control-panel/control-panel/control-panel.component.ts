import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {TeamSocketApiService} from '../../../services/team-socket-api.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.sass']
})
export class ControlPanelComponent implements OnInit {

  @Output() generateAction = new EventEmitter();

  pullingRate = new FormControl(1);
  generateSize = new FormControl(100);


  constructor(
    private teamSocketApi: TeamSocketApiService,
    ) { }

  ngOnInit(): void {
  }

  pullingStart(rate){
    this.teamSocketApi.pullingStart(rate).subscribe( (el: any) => {
      console.log(el.ok ? `Pulling start rate: ${rate}` : null );
    });
  }

  pullingStop(){
    this.teamSocketApi.pullingStop().subscribe( (el: any) => {
      console.log(el.ok ? 'Pulling stop' : null);
    });
  }

  emitGenerateAction(size: number){
    this.pullingStop();
    this.generateAction.emit(size);
  }

}
